# Chartist

Blaze-Компонент реализующий работу графиков [chartist.js](https://gionkunz.github.io/chartist-js/index.html).

### Вызов компонета
```spacebars
{{> Chartist chart}}
```
### Параметры
```javascript
const props = {
  // Тип диаграммы
  type: { type: String, allowedValues: ['line'] },
  // Массив подписей точек
  labels: { type: Array, optional: true },
  // Подпись к точке
  'labels.$': { type: SimpleSchema.oneOf(Number, String) },
  // Массив линий графика
  series: { type: Array },
  // Массив точек линии
  'series.$': { type: SimpleSchema.oneOf(Number, Array) },
  // Значение точки линии
  'series.$.$': { type: Number },
  // опции для графика
  options: { type: Object, blackbox: true },
  // функция для возврата объекта графика
  handleObject: { type: Function, optional: true },
  // класс для обертки графика
  className: { type: String, defaultValue: '' },
}
``` 

