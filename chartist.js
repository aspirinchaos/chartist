import { TemplateController } from 'meteor/template-controller';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import SimpleSchema from 'simpl-schema';
import Chartist from 'chartist';
import './chartist.html';

checkNpmVersions({ 'simpl-schema': '1.5.3' }, 'chartist');

TemplateController('Chartist', {
  props: new SimpleSchema({
    // тип графика
    type: { type: String, allowedValues: ['line'] },
    labels: { type: Array, optional: true },
    'labels.$': { type: SimpleSchema.oneOf(Number, String) },
    series: { type: Array },
    'series.$': { type: SimpleSchema.oneOf(Number, Array) },
    'series.$.$': { type: Number },
    // опции для графика, проверяется самим chatist
    options: { type: Object, blackbox: true, defaultValue: {} },
    // функция для возврата объекта графика
    handleObject: { type: Function, optional: true },
    className: { type: String, defaultValue: '' },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  // нужна работа с DOM, поэтому в onRendered
  onRendered() {
    const { type, handleObject } = this.props;
    // инициируем график в зависимости от типа
    if (type === 'line') {
      this.initLine();
    }
    // Если нужно вернуть график для работы с ним
    if (handleObject) {
      handleObject(this.chart);
    }
    this.autorun(() => {
      const { labels, series, options } = this.props;
      // теперь график обновляется реактивно
      this.chart.update({ labels, series }, options);
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    chart: null,
    initLine() {
      const {
        labels, series, options,
      } = this.props;
      // инициация графика
      this.chart = new Chartist.Line(this.firstNode, {
        labels,
        series,
      }, options);
    },
  },
});
