// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by chartist.js.
import { name as packageName } from "meteor/chartist";

// Write your tests here!
// Here is an example.
Tinytest.add('chartist - example', function (test) {
  test.equal(packageName, "chartist");
});
