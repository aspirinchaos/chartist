Package.describe({
  name: 'chartist',
  version: '0.1.0',
  summary: 'Blaze component for chartist.js',
  git: 'https://bitbucket.org/aspirinchaos/chartist.git',
  documentation: 'README.md',
});

Npm.depends({
  chartist: '0.11.0',
});

Package.onUse((api) => {
  api.versionsFrom('1.7');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'fourseven:scss',
    'templating',
    'template-controller',
  ]);
  api.mainModule('chartist.js', 'client');
  api.addFiles('styles/chartist.scss', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('chartist');
  api.mainModule('chartist-tests.js');
});
